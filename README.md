#REPOSITORY MOVED

This repository has moved to github.com. The link is: (https://github.com/AWAF).

#AWAF (or Aglezabad's Web Application Framework)

Framework for development of client oriented web applications 
using similar structure as Android projects. 
This makes the development of FirefoxOS apps easier because
someone with knowledge about Android development can use almost
same techniques.
