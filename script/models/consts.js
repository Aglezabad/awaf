/*exported consts*/
var Consts = function () {
    'use strict';
    /*File routes*/
	this.SCRIPT_ROOT = 'script/';
	this.STYLE_ROOT = 'style/';
	this.LAYOUT_ROOT = 'layout/';
	this.CONTROLLERS_ROUTE = this.SCRIPT_ROOT + 'controllers/';
	this.MODELS_ROUTE = this.SCRIPT_ROOT + 'models/';
	this.SCRIPT_PAGES_ROUTE = this.SCRIPT_ROOT + 'pages/';
	this.STYLE_PAGES_ROUTE = this.STYLE_ROOT + 'pages/';
	this.LAYOUT_PAGES_ROUTE = this.LAYOUT_ROOT + 'pages/';
    /*Application parameters*/
    this.PAGES_LOCAL = false;
    this.XHR_ASYNC = true;
};

var consts = new Consts();

