/*jshint bitwise: true, eqeqeq: true, es5: true, undef: true, unused: true*/
/*jslint browser: true, devel: true*/
/*exported domUtils*/
var DomUtils = function () {
    'use strict';
	this.ready = function (callback) {
		if (document.readyState !== 'loading') {
            callback();
        } else {
            document.addEventListener('DOMContentLoaded', callback);
        }
    };
};

var domUtils = new DomUtils();