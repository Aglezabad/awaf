/*jshint bitwise: true, eqeqeq: true, es5: true, undef: true, unused: true*/
/*jslint browser: true, devel: true*/
/*exported errHandler*/
var ErrHandler = function () {
    'use strict';
    var self = this;
    this.createErrorHandler = function () {
        window.onerror = self.createErrorWindow;
    };
    this.createErrorWindow = function (errorMsg, url, lineNumber, column, errorObj) {
        alert('Error: ' + errorMsg + ' Script: ' + url + ' Line: ' + lineNumber + ' Column: ' + column + ' StackTrace: ' +  errorObj);
    };
};

var errHandler = new ErrHandler();