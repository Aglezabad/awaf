/*jshint bitwise: true, eqeqeq: true, es5: true, undef: true, unused: true*/
/*jslint browser: true, devel: true*/
/*globals consts*/
/*exported loader*/
var Loader = function () {
    'use strict';
	this.isLoaded = function (filename) {
		var filenameData = filename.split('.'),
            elementList,
            loaded = false,
            i;
		switch (filenameData[filenameData.length - 1]) {
        case 'js':
            elementList = document.getElementsByTagName('script');
			for (i = elementList.length - 1; i >= 0; i -= 1) {
				if (elementList[i].getAttribute('src').indexOf(filenameData[0]) > -1) {
					loaded = true;
				}
			}
            break;
        case 'css':
            elementList = document.getElementsByTagName('link');
			for (i = elementList.length - 1; i >= 0; i -= 1) {
				if (elementList[i].getAttribute('href').indexOf(filenameData[0]) > -1) {
					loaded = true;
				}
			}
			break;
        case 'html':
            elementList = document.getElementById(filenameData[0]);
			if (elementList !== null) {
				loaded = true;
			}
			break;
        default:
            break;
        }
		return loaded;
	};

	this.unload = function (route, filename, callback) {
		var filenameData = filename.split('.'),
            self = this,
            elementList,
            i;
		if (self.isLoaded(filename)) {
			switch (filenameData[filenameData.length - 1]) {
            case 'js':
				elementList = document.getElementsByTagName('script');
				for (i = elementList.length - 1; i >= 0; i -= 1) {
					if (elementList[i].getAttribute('src').indexOf(filenameData[0]) > -1) {
						elementList[i].parentNode.removeChild(elementList[i]);
					}
				}
				break;
            case 'css':
				elementList = document.getElementsByTagName('link');
				for (i = elementList.length - 1; i >= 0; i -= 1) {
					if (elementList[i].getAttribute('href').indexOf(filenameData[0]) > -1) {
						elementList[i].parentNode.removeChild(elementList[i]);
					}
				}
				break;
			case 'html':
				elementList = document.getElementById(filenameData[0]);
				elementList.innerHTML = '';
				elementList.removeAttribute('id');
				break;
			default:
				callback(1);
				break;
			}
			callback(0);
		} else {
			callback(-1);
		}
	};

	this.load = function (route, filename, local, callback) {
		var filenameData = filename.split('.'),
            self = this,
            element,
            xhr;
		local = typeof local !== 'undefined' ? local : false;
		if (!self.isLoaded(filename)) {
			switch (filenameData[filenameData.length - 1]) {
            case 'js':
				element = document.createElement('script');
				element.setAttribute('type', 'text/javascript');
				element.setAttribute('src', route + filename);
				element.onload = function () {
					callback(0);
				};
				document.body.appendChild(element);
				break;
			case 'css':
				element = document.createElement('link');
				element.setAttribute('type', 'text/css');
				element.setAttribute('rel', 'stylesheet');
				element.setAttribute('href', route + filename);
				element.onload = function () {
					callback(0);
				};
                document.head.appendChild(element);
				break;
			case 'html':
				element = document.getElementsByClassName('content')[0];
				xhr = new XMLHttpRequest();
				xhr.open('GET', route + filename, consts.XHR_ASYNC);
				xhr.onreadystatechange = function () {
					if (xhr.readyState === 4) {
						if (local || xhr.status === 200) {
							element.innerHTML = xhr.responseText;
							element.setAttribute('id', filenameData[0]);
							callback(0);
						} else {
							callback(-1);
						}
					}
				};
				xhr.send();
				break;
			default:
				callback(1);
				break;
            }
		} else {
			callback(-1);
		}
	};

	this.loadPage = function (pagename, callback) {
		var self = this;
		self.load(consts.LAYOUT_PAGES_ROUTE, pagename + '.html', consts.LOCAL_PAGES, function (status) {
			if (status !== 0) {
				callback(-1);
			} else {
				self.load(consts.STYLE_PAGES_ROUTE, pagename + '.css', consts.LOCAL_PAGES, function (status) {
					if (status !== 0) {
						callback(-2);
					} else {
						self.load(consts.SCRIPT_PAGES_ROUTE, pagename + '.js', consts.LOCAL_PAGES, function (status) {
							if (status !== 0) {
								callback(-3);
							} else {
								callback(0);
							}
						});
					}
				});
			}
		});
	};

	this.unloadPage = function (pagename, callback) {
		var self = this;
		self.unload(consts.LAYOUT_PAGES_ROUTE, pagename + '.html', function (status) {
			if (status !== 0) {
				callback(-1);
			} else {
				self.unload(consts.STYLE_PAGES_ROUTE, pagename + '.css', function (status) {
					if (status !== 0) {
						callback(-2);
					} else {
						self.unload(consts.SCRIPT_PAGES_ROUTE, pagename + '.js', function (status) {
							if (status !== 0) {
								callback(-3);
							} else {
								callback(0);
							}
						});
					}
				});
			}
		});
	};
};

var loader = new Loader();