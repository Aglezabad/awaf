/*jshint bitwise: true, eqeqeq: true, es5: true, undef: true, unused: true*/
/*jslint browser: true, devel: true*/
/*globals domUtils, loader*/
function loadIndexPage() {
    'use strict';
    loader.unloadPage('splash', function (status) {
        if (status !== 0) {
            throw new Error("Couldn't unload actual page components. Status: " + status);
        } else {
            loader.loadPage('index', function (status) {
                if (status !== 0) {
                    throw new Error("Page components not loaded. Status: " + status);
                }
            });
        }
    });
}


domUtils.ready(function () {
    'use strict';
    setTimeout(loadIndexPage, 2000);
});