/*jshint bitwise: true, eqeqeq: true, es5: true, undef: true, unused: true*/
/*jslint browser: true, devel: true*/
/*globals loader, domUtils, errHandler*/
var Main = function () {
    'use strict';
	this.init = function () {
        errHandler.createErrorHandler();
		loader.loadPage('splash', function (status) {
			if (status !== 0) {
				throw new Error("Page components not loaded.");
			}
		});
	};
};

var main = new Main();
domUtils.ready(main.init);